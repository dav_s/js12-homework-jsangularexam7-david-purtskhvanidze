import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-add-items',
  templateUrl: './add-items.component.html',
  styleUrls: ['./add-items.component.css']
})
export class AddItemsComponent {
  @Input() name = '';
  @Input() icon = '';
  @Input() price = 0;
  @Output() add = new EventEmitter();

  onClickAdd() {
    this.add.emit();
  };
}
