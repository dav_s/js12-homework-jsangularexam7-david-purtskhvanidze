import { Component } from '@angular/core';
import { Item } from "./shared/item.model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  items: Item[] = [
    new Item('coffee','Coffee', 0, 180),
    new Item('tea','Tea', 0, 120),
    new Item('sandwich','Sandwich', 0, 140),
    new Item('ice-cream','Ice Cream', 0, 125),
    new Item('muffin','Muffin', 0, 90),
    new Item('toast','Toast', 0, 80),
  ];

  addItem(i: number) {
    this.items[i].count = this.items[i].count + 1;
  };

  deleteItem(i: number) {
    this.items[i].count = 0;
  };

  getTotalPrice() {
    return this.items.reduce((acc,item) => {
      return acc + item.getPrice();
    }, 0);
  };

  getTotalCount() {
    return this.items.reduce((acc,item) => {
      return acc + item.count;
    }, 0);
  };
}
